**Summary:** [short description of problem here]

**Steps to reproduce:**

1. [first step]
2. [second step]
3. [other steps...]

**Expected behavior:** [describe expected behavior here]

**Observed behavior:**  [describe observed behavior here]

**Notes:** [additionnal notes here]

**Reported by:** [if not you]

**Additional information:**

* Problem can be reproduced: [yes/no]
* Problem can be reliably reproduced, doesn't happen randomly: [yes/no]